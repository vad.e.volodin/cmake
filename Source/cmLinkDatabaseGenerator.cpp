#include "cmLinkDatabaseGenerator.h"

void cmLinkDatabaseGenerator::GenerateLinkCommand(
  const std::vector<std::string>& dependencyFiles,
  const std::string& workingDirectory,
  const std::vector<std::string>& linkCommands, const std::string& version,
  bool isOnOption, std::set<int> ranlibIndexes,
  const std::string& buildFileDir)
{

  std::string linkCommandsFile = cmStrCat("/", "link_commands.json");

  if (!LinkCommandsJson) {
    std::string commandDatabaseName = cmStrCat(buildFileDir, linkCommandsFile);

    Json::Value jsonVersionValue;
    if (!version.empty()) {
      if (isOnOption) {
        jsonVersionValue["version"] = newestSupportedFormat;
      } else {
        jsonVersionValue["version"] = version;
      }
    } else {
      return;
    }

    LinkCommandStream =
      cm::make_unique<cmGeneratedFileStream>(commandDatabaseName);
    LinkCommandsJson.append(jsonVersionValue);
  }

  Json::Value linkJsonCommand;
  linkJsonCommand["directory"] =
    cmGlobalGenerator::EscapeJSON(workingDirectory);

  Json::Value files(Json::arrayValue);
  for (const auto& sourceFile : dependencyFiles) {
    files.append(Json::Value(cmGlobalGenerator::EscapeJSON(sourceFile)));
  }
  linkJsonCommand["files"] = files;

  for (size_t i = 0; i < linkCommands.size(); ++i) {
    // skip ranlib command
    if (ranlibIndexes.find(i) == ranlibIndexes.end()) {
      linkJsonCommand["command"] =
        cmGlobalGenerator::EscapeJSON(linkCommands[i]);
      LinkCommandsJson.append(linkJsonCommand);
    }
  }
}

void cmLinkDatabaseGenerator::CloseLinkCommandsStream()
{
  if (LinkCommandStream) {
    *LinkCommandStream << LinkCommandsJson.toStyledString();
    LinkCommandStream.reset();
  }
}
cmLinkDatabaseGenerator::cmLinkDatabaseGenerator() = default;
